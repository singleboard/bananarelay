# BananaRelay
*Relay control system with sensor modules DHT22, DS18B20*

![image](images/img.png)

## Requirements

**Hardware**
> BananaPI P2 Zero (BPI-P2 Zero) \
> Banana Pi BPI-9600 PoE Module (for BPI-P2 Zero) (optional)


**Software**
> OS: debian [raspbian stretch 2020-04-28](https://drive.google.com/file/d/1G5NJDwL0Vs9YLwUEdGXMSwBPqRx6CxTC/view?usp=sharing)
> Images for m2z and p2z board are compatible
> 
> Services: 
> * mosquitto (mqtt server)
> * node-red (frontend)
> * nginx (reverse-proxy for node-red)
> 
> C++ libraries:
> * wiringPi
> * paho-mqtt
> * jsoncpp
> * glog
> * gflags

## Install requirements

Install Mosquitto, node-red and nginx services as usual

## Configure nginx reverse proxy for node-red
Get conf from 
[conf-nginx-proxy](./scripts/services/nginx-proxy.conf)


## Clone, build, install project
[How to step by step](./docs/README_zero.md)

## Build
Run script `./script/build.sh`

## Install services
Run script `./script/install-service.sh`

## Uninstall services
Run script `./script/uninstall-service.sh`


Enjoy services
```bash
systemctl status banana-ds18b20.service
systemctl status banana-dht22.service
systemctl status banana-relay.service
```

> Note: banana-relay.service starts with 30 sec delay

## Links
[Setup node-red dashboard](http://{IP}:1880) \
[Node-red dashboard](http://{IP}:1880/ui) \
[MQTT server](http://{IP}:1883)


## Useful info:
### Remote Debug (over ssh)
[Setup remote debug on CLion](docs/README_remote-debug.md)

### ui-time-scheduler locations
`/home/pi/.node-red/node_modules/node-red-contrib-ui-time-scheduler/locales/en-US/time-scheduler.json`

### Add systemd timer
[Systemd timers](https://wiki.archlinux.org/index.php/Systemd/Timers)
```text
systemctl list-timers
[Timer]
OnBootSec=15min
OnUnitActiveSec=1w 
```
