# Cpp libs

## JsonCpp
### Install
```bash
apt-get install libjsoncpp-dev
```
### Using
add `-ljsoncpp`

`g++ -o profile profile.cpp -ljsoncpp`


## Google-glog
[How to use](https://programming.vip/docs/basic-usage-of-glog.html)

### 1 way
```bash
apt install libgoogle-glog-dev
```
then add lines into `CMakeLists.txt`
```txt
target_link_libraries(banana-relay glog)
```

### 2 way
Download release and unpack into `libs` \
[https://gflags.github.io/gflags/](https://gflags.github.io/gflags/)

then add lines into `CMakeLists.txt`
```txt
add_subdirectory(libs/glog-0.4.0)

target_link_libraries(banana-relay glog)
```

## Google-gflags
[How to use](http://rpg.ifi.uzh.ch/docs/glog.html)
[How to use](https://gflags.github.io/gflags/)
[How to use](http://lars.mec.ua.pt/public/LAR%20Projects/RobotManipulation/2016_TiagoSimoes/Programacao/kinect_sensor_rep/instalar/gflags-2.1.2/doc/)


### 1 way
```bash
apt install libgflags-dev
```
then add lines into `CMakeLists.txt`
```txt
target_link_libraries(banana-relay gflags)
```


### 2 way
Download release and unpack into `libs` \
[https://gflags.github.io/gflags/](https://gflags.github.io/gflags/)


then add lines into `CMakeLists.txt`
```txt
add_subdirectory(libs/gflags-2.2.2)

target_link_libraries(banana-relay gflags)
```




