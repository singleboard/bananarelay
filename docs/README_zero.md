# 
user: pi
password: bananapi

user: root
password: bananapi

```bash
sudo apt update
sudo apt upgrade
sudo reboot

```

Use root user
change in `/etc/ssh/sshd_config`
```
#PermitRootLogin prohibit-password
PermitRootLogin yes
```
```bash
/etc/init.d/ssh restart
```
Login as `root`

## Install mqtt (mosquitto)
```bash
apt-get install mosquitto mosquitto-clients
```

## Install node-red (with nodejs)
[how to install](https://nodered.org/docs/getting-started/raspberrypi)
```bash
bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
```

## Install libs
```bash
apt install libjsoncpp-dev
apt install libgoogle-glog-dev
apt install libgflags-dev
```

## Build and install WiringPi

```bash
# http://forum.banana-pi.org/t/banana-pi-m2-zero-wiringpi2/5517/7
git clone https://github.com/BPI-SINOVOIP/BPI-WiringPi2
cd BPI-WiringPi2/
sudo su
# check env
cat /var/lib/bananapi/board.sh
# BOARD=bpi-m2z
# BOARD_AUTO=bpi-m2z
# BOARD_OLD=bpi-m2u
sudo ./build
#  will be many warnings

# Testing BPI-WiringPi2
gpio readall
```

## Build and install PahoMQTT
https://www.eclipse.org/paho/files/mqttdoc/MQTTClient/html/index.html

https://github.com/eclipse/paho.mqtt.cpp
https://www.youtube.com/watch?v=JZb7wJmGpgs

```bash
apt update
apt-get install build-essential gcc make cmake cmake-qt-gui cmake-curses-gui git doxygen graphviz libssl-dev
git clone https://github.com/eclipse/paho.mqtt.c.git
cd paho.mqtt.c
git checkout v1.3.1
cmake -Bbuild -H. -DPAHO_WITH_SSL=ON
cmake --build build/ --target install
ldconfig
cd ..
git clone https://github.com/eclipse/paho.mqtt.cpp.git
cd paho.mqtt.cpp
git checkout v1.0.1
cmake -Bbuild -H. -DPAHO_BUILD_DOCUMENTATION=TRUE -DPAHO_BUILD_SAMPLES=TRUE
cmake --build build/ --target install
ldconfig
```

```bash
mkdir Projects
cd Projects
./scripts/build.sh
./scripts/install-service.sh
reboot
```


## Install Node-Red nodes

* node-red-dashboard
* node-red-contrib-ui-time-scheduler

and import `./apps/node-red/node-red-flows.json`



