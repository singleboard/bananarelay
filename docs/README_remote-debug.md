# Remote debug with CLon

[CLION remote debug](https://www.jetbrains.com/help/clion/remote-projects-support.html#excluded-paths)
- Config `Clion -> Preferences -> Build, Execution, Deployment -> Toolchain (Remote host)`
- Config `Clion -> Preferences -> Build, Execution, Deployment -> CMake (Debug remote)`

Find auto created item in `Deployment` and change value `Mapping -> Deployment path`
- Clion -> Preferences -> Build, Execution, Deployment -> Deployment

> Because default folder locates in /tmp and it is removed after reboot 