// systemctl restart ds18b20.service
// g++ -Wall main.cpp OneWire.cpp -o ds18b20 -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi
// ./ds18b20 tcp://localhost:1883 DS18B20 5000
// ./ds18b20 DFLT_SERVER_ADDRESS DFLT_CLIENT_ID DFLT_DELAY_MILLISEC

#include <glog/logging.h>
#include <gflags/gflags.h>

#include "OneWire.h"


#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>	// For sleep
#include <atomic>
#include <chrono>
#include <cstring>

#include "mqtt/async_client.h"

using namespace std;

DEFINE_string(mqtt_server, "tcp://localhost:1883", "Define mqtt server address 'tcp://localhost:1883'");
DEFINE_string(mqtt_client_id, "ds18b20", "Define mqtt client id, must be unique");
DEFINE_int32(wiringpi_pin, 29, "Define wiringPi number for signal");
DEFINE_int32(sensor_quantity, 2, "Amount ot DS18B20 sensors");
DEFINE_string(topic_prefix, "BANANA", "Define wiringPi number for signal");
DEFINE_int32(sleep_time_sec, 5, "Define time interval between measurements in seconds");

DEFINE_string(log_file_prefix, "/var/log/banana-relay/", "Define log_file_prefix");

const auto TIMEOUT = std::chrono::seconds( 10);

const int  QOS = 1;

int main(int argc, char* argv[])
{
    gflags::ParseCommandLineFlags(&argc, const_cast<char***>(&argv), true);
//    FLAGS_logtostderr=1;
    FLAGS_alsologtostderr=1;
    FLAGS_logbufsecs = 0;
    google::InitGoogleLogging(to_string( FLAGS_alsologtostderr).c_str() );
    if(FLAGS_log_dir.empty())
        FLAGS_log_dir = "/var/log/banana-relay";
    //Sets the output directory and prefix for logs of a specific severity level. The first parameter is the log level, and the second parameter indicates the output directory and log filename prefix
    google::SetLogDestination(google::GLOG_INFO, (string(FLAGS_log_dir) + "/banana-ds18b20_info_").c_str());
    google::SetLogDestination(google::GLOG_FATAL, (string(FLAGS_log_dir) + "/banana-ds18b20_error_").c_str());
    //Add an extension after the middle level of the log file name. For all severity levels
    google::SetLogFilenameExtension("log_");
    //Logs larger than the specified level are output to standard output
    google::SetStderrLogging(google::GLOG_ERROR);
    FLAGS_colorlogtostderr = true;  // Set log color
    FLAGS_logbufsecs = 0;  // Set log output speed(s)
    FLAGS_max_log_size = 1024;  // Set max log file size
    FLAGS_stop_logging_if_full_disk = true;  // If disk is full

    LOG(INFO) << "FLAGS_log_dir" << FLAGS_log_dir;
    LOG(INFO) << "FLAGS_mqtt_server" << FLAGS_mqtt_server;
    LOG(INFO) << "FLAGS_mqtt_client_id" << FLAGS_mqtt_client_id;
    LOG(INFO) << "FLAGS_wiringpi_pin" << FLAGS_wiringpi_pin;
    LOG(INFO) << "FLAGS_sensor_quantity" << FLAGS_sensor_quantity;
    LOG(INFO) << "FLAGS_topic_prefix" << FLAGS_topic_prefix;
    LOG(INFO) << "FLAGS_sleep_time_sec" << FLAGS_sleep_time_sec;

    int N = FLAGS_sensor_quantity;

    this_thread::sleep_for(chrono::seconds(1));
	OneWire * ds18b20;
	uint64_t roms[N];
	try {
        ds18b20 = new OneWire(  FLAGS_wiringpi_pin);
		ds18b20->oneWireInit();
		ds18b20->searchRom(roms, N);
        LOG(INFO) << "---------------------------------" << endl;
		for (int i = 0; i < N; i++) {
			LOG(INFO) << "addr T[" << (i + 1) << "] = " << roms[i] << endl;
		}
		LOG(INFO) << "---------------------------------" << endl;
	} catch (exception & e) {
		LOG(FATAL) << "OneWire problem" << e.what() << endl;
		return 1;
	}

	try {
        LOG(INFO) << "Initializing for server '" << FLAGS_mqtt_server << "'..." << endl;
        mqtt::async_client client(FLAGS_mqtt_server, FLAGS_mqtt_client_id);
        mqtt::connect_options conopts;
        LOG(INFO) << "...OK" << endl;
		LOG(INFO) << "Connecting..." << endl;
		mqtt::token_ptr conntok;

		try {
            conntok = client.connect(conopts);
            LOG(INFO) << "Waiting for the connection..." << endl;
            conntok->wait();
            LOG(INFO) << "  ...OK" << endl;
        } catch (exception & e) {
            LOG(FATAL) << "ERROR: MQTT connection " << e.what() << endl;
            return 1;
        }

		while(1) {
			for (int i = 0; i < N; i++) {
				double temperature;
				temperature = OneWire::getTemp(ds18b20, roms[i]);
				char dest[40];
				sprintf(dest, "%.2f", temperature);
				string topic_temp = FLAGS_topic_prefix + "/DS18B20/" + to_string(i) + "/TEMP";
				LOG(INFO) << "Sending message" << " TOPIC:" << topic_temp << ":" << dest << endl;
				mqtt::message_ptr pubmsg = mqtt::make_message(topic_temp, dest);
				pubmsg->set_qos(QOS);
				try {
                    client.publish(pubmsg)->wait_for(TIMEOUT);
				}
				catch (const exception& ex) {
                    LOG(ERROR) << "Error: " << ex.what() << endl;
				}
				LOG(INFO) << "  ...OK" << endl;

				string mes = to_string(roms[i]);
                string topic_id = FLAGS_topic_prefix + "/DS18B20/" + to_string(i) + "/ID";
                LOG(INFO) << "Sending message" << " TOPIC:" << topic_id << ":" << mes << endl;
                mqtt::message_ptr pubmsg2 = mqtt::make_message(topic_id, mes.c_str());
                pubmsg2->set_qos(QOS);
                try {
                    client.publish(pubmsg2)->wait_for(TIMEOUT);
                }
                catch (const exception& ex) {
                    LOG(ERROR) << "Error: " << ex.what() << endl;
                }
                LOG(INFO) << "  ...OK" << endl;

			}
            LOG(INFO) << "Waiting..." << FLAGS_sleep_time_sec << " seconds" << endl;
			this_thread::sleep_for(chrono::seconds(FLAGS_sleep_time_sec));
        }

		// Disconnect
		LOG(INFO) << "\nDisconnecting..." << endl;
		conntok = client.disconnect();
		conntok->wait();
		LOG(INFO) << "  ...OK" << endl;

        google::ShutdownGoogleLogging();
	}
	catch (const mqtt::exception& exc) {
		LOG(ERROR) << exc.what() << endl;

        google::ShutdownGoogleLogging();
		return 1;
	}

 	return 0;
}

