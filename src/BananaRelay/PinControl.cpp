//
// Created by Antonchikov Alexander on 29.01.2021.
//

#include "PinControl.h"
#include "BananaState.h"
#include <wiringPi.h>

int PinControl::initWiringPi() {
    pinMode(PIN_FREE, OUTPUT);
    pinMode(PIN_COMPRES, OUTPUT);
    pinMode(PIN_COND, OUTPUT);
    pinMode(PIN_FAN, OUTPUT);
    pinMode(PIN_PUMP, OUTPUT);
    pinMode(PIN_LAMP, OUTPUT);
    return 0;
}

int PinControl::updateWiringPi(std::string topic, std::string message, BananaState* bananaState) {

    if(topic == bananaState->TOPIC_PREFIX + "/LAMP") {
        if(message == "on") {
            digitalWrite(PIN_LAMP, HIGH);
            bananaState->PIN_LAMP_STATE = true;
        }
        else if(message == "off") {
            digitalWrite(PIN_LAMP, LOW);
            bananaState->PIN_LAMP_STATE = false;
        }
    }
    if(topic == bananaState->TOPIC_PREFIX + "/PUMP") {
        if(message == "on") {
            digitalWrite(PIN_PUMP, HIGH);
            bananaState->PIN_PUMP_STATE = true;
        }
        else if(message == "off") {
            digitalWrite(PIN_PUMP, LOW);
            bananaState->PIN_PUMP_STATE = false;
        }
    }
    if(topic == bananaState->TOPIC_PREFIX + "/FAN") {
        if(message == "on") {
            digitalWrite(PIN_FAN, HIGH);
            bananaState->PIN_FAN_STATE = true;
        }
        else if(message == "off") {
            digitalWrite(PIN_FAN, LOW);
            bananaState->PIN_FAN_STATE = false;
        }
    }
    if(topic == bananaState->TOPIC_PREFIX + "/COND") {
        if(message == "on") {
            digitalWrite(PIN_COND, HIGH);
            bananaState->PIN_COND_STATE = true;
        }
        else if(message == "off") {
            digitalWrite(PIN_COND, LOW);
            bananaState->PIN_COND_STATE = false;
        }
    }
    if(topic == bananaState->TOPIC_PREFIX + "/COMPRES") {
        if(message == "on") {
            digitalWrite(PIN_COMPRES, HIGH);
            bananaState->PIN_COMPRES_STATE = true;
        }
        else if(message == "off") {
            digitalWrite(PIN_COMPRES, LOW);
            bananaState->PIN_COMPRES_STATE = false;
        }
    }
    if(topic == bananaState->TOPIC_PREFIX + "/FREE") {
        if(message == "on") {
            digitalWrite(PIN_FREE, HIGH);
            bananaState->PIN_FREE_STATE = true;
        }
        else if(message == "off") {
            digitalWrite(PIN_FREE, LOW);
            bananaState->PIN_FREE_STATE = false;
        }
    }
    return 0;
}