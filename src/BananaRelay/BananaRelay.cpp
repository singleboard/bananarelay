// systemctl restart ds18b20.service
// g++ -Wall src/BananaRelay.cpp src/MyTime.cpp src/BananaState.cpp src/PinControl.cpp -o bin/BananaRelay -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi -ljsoncpp

#include <glog/logging.h>
#include <gflags/gflags.h>

#include <wiringPi.h>
#include "BananaState.h"
#include "PinControl.h"

#include "MyTime.h"

#include <fstream>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.

#include <iostream>
#include <cstdlib>
#include <string>
#include <cstring>
#include <cctype>
#include <thread>
#include <chrono>
#include "mqtt/async_client.h"

#include <algorithm>    // copy
#include <iterator>     // back_inserter
#include <regex>        // regex, sregex_token_iterator
#include <vector>
#include <list>
#include <ctime>
#include <chrono>
#include <cmath>

DEFINE_string(mqtt_server, "tcp://localhost:1883", "Define mqtt server address 'tcp://localhost:1883'");
DEFINE_string(mqtt_client_id, "async_consume", "Define mqtt client id, must be unique");
DEFINE_string(topic_prefix, "BANANA", "Define wiringPi number for signal");
DEFINE_string(state_file, "/etc/banana-relay/banana-state.json", "Define state file location for saving current state" );

using namespace std;
using namespace chrono;

const int  QOS = 1;

/////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    gflags::ParseCommandLineFlags(&argc, const_cast<char***>(&argv), true);
//    FLAGS_logtostderr=1;
    FLAGS_alsologtostderr=1;
    FLAGS_logbufsecs = 0;
    google::InitGoogleLogging(to_string( FLAGS_alsologtostderr).c_str() );
    if(FLAGS_log_dir.empty())
        FLAGS_log_dir = "/var/log/banana-relay";
    //Sets the output directory and prefix for logs of a specific severity level. The first parameter is the log level, and the second parameter indicates the output directory and log filename prefix
    google::SetLogDestination(google::GLOG_INFO, (string(FLAGS_log_dir) + "/banana-relay_info_").c_str());
    google::SetLogDestination(google::GLOG_FATAL, (string(FLAGS_log_dir) + "/banana-relay_error_").c_str());
    //Add an extension after the middle level of the log file name. For all severity levels
    google::SetLogFilenameExtension("log_");
    //Logs larger than the specified level are output to standard output
    google::SetStderrLogging(google::GLOG_ERROR);
    FLAGS_colorlogtostderr = true;  // Set log color
    FLAGS_logbufsecs = 0;  // Set log output speed(s)
    FLAGS_max_log_size = 1024;  // Set max log file size
    FLAGS_stop_logging_if_full_disk = true;  // If disk is full

    LOG(INFO) << "FLAGS_mqtt_server" << FLAGS_mqtt_server;
    LOG(INFO) << "FLAGS_mqtt_client_id" << FLAGS_mqtt_client_id;
    LOG(INFO) << "FLAGS_topic_prefix" << FLAGS_topic_prefix;
    LOG(INFO) << "FLAGS_state_file" << FLAGS_state_file;

    const string TOPIC 			{ FLAGS_topic_prefix + "/#" };

    LOG(INFO) << "Banana relay started";
    BananaState bState(FLAGS_topic_prefix);

    if (wiringPiSetup() == -1) { //If wiringpi initialization failed, printf
        LOG(FATAL) << "Wiring Pi Initialization Failed";
        return 1;
    }

    PinControl::initWiringPi();

    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(20);
    connOpts.set_clean_session(true);
    mqtt::async_client cli(FLAGS_mqtt_server, FLAGS_mqtt_client_id);

    try {
        LOG(INFO) << "Connecting to the MQTT server..." << flush;
        cli.connect(connOpts)->wait();
        cli.start_consuming();
        cli.subscribe(TOPIC, QOS)->wait();
        LOG(INFO) << "OK" << endl;
    }
    catch (const mqtt::exception& exc) {
        LOG(FATAL) << exc.what() << endl;
        return 1;
    }

    bState.initMqtt(&cli);

    high_resolution_clock::time_point t_start = high_resolution_clock::now();
    try {
        while (true) {
            high_resolution_clock::time_point t_current = high_resolution_clock::now();
            duration<double> time_span = duration_cast<duration<double>>(t_current - t_start);
            double currentDuration = time_span.count();
            if(currentDuration > 10) {
                bState.log();
                t_start = high_resolution_clock::now();
            }


            auto msg = cli.consume_message();
            if (!msg) break;
            string topic = msg->get_topic();
            string message = msg->to_string();

            stringstream streamTopic(topic.c_str());
            string segment;
            vector<string> vectorTopic;

            while(std::getline(streamTopic, segment, '/'))
            {
                vectorTopic.push_back(segment);
            }
//            LOG(INFO) << vectorTopic.at(0) << "/" << vectorTopic.at(1) << ":" << message << endl;
//            PinControl::updateWiringPi(vectorTopic.at(1), message);

            // Make phisical changes
//            PinControl::updateWiringPi(topic, message, &bState);
            // Make changes in state here BananaState
            bState.updateState(topic, message);

//            ------------------------------
//            PUMP
            if (bState.tLAMP < bState.tLAMP_MIN) {
                if (bState.PIN_PUMP_STATE)
                    bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/PUMP" }, { "off" });
            }
            else if(bState.tLAMP > bState.tLAMP_MAX) {
                if (!bState.PIN_PUMP_STATE)
                    bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/PUMP" }, { "on" });
            }

//            -------------------------------
//            LAMP
            if (bState.tLAMP > bState.tLAMP_ALARM) {
                if (bState.PIN_LAMP_STATE)
                    bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/LAMP" }, { "off" });
            }
//            !!!!IMPORTANT
            else if (bState.listTIMER.size() > 0) {
                std::time_t t = std::time(0);   // get time now
                std::tm* now = std::localtime(&t);
    //            std::LOG(INFO) << (now->tm_hour) << '-'
    //                      << (now->tm_min) << '-'
    //                      <<  now->tm_sec
    //                      << "\n";
                MyTimeClass* currentTime = new MyTimeClass(now->tm_hour, now->tm_min, now->tm_sec);
                list <MyTimeRangeClass*> :: iterator it;
                bool flag = false;
                for (it = bState.listTIMER.begin(); it != bState.listTIMER.end(); it++) {
                    MyTimeRangeClass* timeRange = (*it);
                    if(timeRange->contain(currentTime)) {
                        flag = true;
                    }
                }
                if(flag) {
                    if (!bState.PIN_LAMP_STATE)
                        bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/LAMP" }, { "on" });
                }
                else {
                    if (bState.PIN_LAMP_STATE)
                        bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/LAMP" }, { "off" });
                }
            }

//            --------------------------------
//            FAN and COND
            if (bState.tIN < bState.tIN_MIN) {
                if (bState.PIN_FAN_STATE)
                    bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/FAN" }, { "off" });
                if (bState.PIN_COND_STATE)
                    bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/COND" }, { "off" });
            }
            else if (bState.tIN > bState.tIN_MAX) {
                if (!bState.PIN_FAN_STATE)
                    bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/FAN" }, { "on" });
                if (!bState.PIN_COND_STATE && bState.tOUT > bState.tOUT_POROG)
                    bState.sendMqtt(&cli, { FLAGS_topic_prefix + "/COND" }, { "on" });
            }

//            ------------------------------
//            FREE
            if (bState.FREE_START) {
                high_resolution_clock::time_point t2 = high_resolution_clock::now();
                duration<double> time_span = duration_cast<duration<double>>(t2 - bState.t1);
                double currentDuration = time_span.count();
                double remains = fmod(currentDuration, (bState.secFREE_ON + bState.secFREE_OFF));
                if (remains < bState.secFREE_ON) {
                    if (!bState.PIN_FREE_STATE)
                        bState.sendMqtt(&cli, {FLAGS_topic_prefix + "/FREE"}, {"on"});
                } else {
                    if (bState.PIN_FREE_STATE)
                        bState.sendMqtt(&cli, {FLAGS_topic_prefix + "/FREE"}, {"off"});
                }
            } else {
                if (bState.PIN_FREE_STATE && (topic == FLAGS_topic_prefix + "/FREE_START" && message=="off"))
                    bState.sendMqtt(&cli, {FLAGS_topic_prefix + "/FREE"}, {"off"});
            }

            this_thread::sleep_for(chrono::milliseconds(10));
        }

        // Disconnect
        LOG(INFO) << "\nShutting down and disconnecting from the MQTT server..." << flush;
        cli.unsubscribe(TOPIC)->wait();
        cli.stop_consuming();
        cli.disconnect()->wait();
        LOG(INFO) << "OK" << endl;
    }
    catch (const mqtt::exception& exc) {
        LOG(FATAL) << exc.what() << endl;
        return 1;
    }
    catch (const runtime_error& exc) {
        LOG(FATAL) << exc.what() << endl;
        return 1;
    }

    return 0;
}

