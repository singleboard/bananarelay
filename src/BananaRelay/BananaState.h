//
// Created by Antonchikov Alexander on 29.01.2021.
//

#ifndef BANANARELAY_BANANASTATE_H
#define BANANARELAY_BANANASTATE_H

#include <iostream>
#include <map>
#include <list>
#include <chrono>

#include <sys/types.h>
#include <sys/stat.h>
#include <exception>

#include "mqtt/async_client.h"
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.
#include "MyTime.h"
#include <glog/logging.h>


#define PIN_FREE 0  // BANANA/FREE
#define PIN_COMPRES 2 // BANANA/COMPRES
#define PIN_COND 3  // BANANA/COND
#define PIN_FAN 22  // BANANA/FAN
#define PIN_PUMP 23 // BANANA/PUMP
#define PIN_LAMP 24 // BANANA/LAMP

using namespace std;
using namespace chrono;


static std::map<char, std::string> MQTT_RELAY_TOPIC = {
        { PIN_FREE, "/FREE" },
        { PIN_COMPRES, "/COMPRES" },
        { PIN_COND, "/COND" },
        { PIN_FAN, "/FAN" },
        { PIN_PUMP, "/PUMP" },
        { PIN_LAMP, "/LAMP" }
};

class BananaState {


public:
    string STATE_FILE = "/etc/banana-relay/banana-state.json";
    string TOPIC_PREFIX = "";
    Json::Value root;

    bool PIN_LAMP_STATE;
    bool PIN_PUMP_STATE;
    bool PIN_FAN_STATE;
    bool PIN_COND_STATE;
    bool PIN_COMPRES_STATE;
    bool PIN_FREE_STATE;

    double tLAMP_MAX = 40;  // PUMP switch on when tLAMP > TEMP_LAMP_MAX
    double tLAMP_MIN = 20;  // PUMP switch off when tLAMP < tLAMP_MIN
    double tLAMP_ALARM = 75;  // LAMP switch off when tLAMP < tLAMP_ALARM
    double tLAMP = 0;
    double tIN_MAX = 40;    //
    double tIN_MIN = 20;
    double tIN = 0;         // DHT22 temperature
    double hIN = 0;         // DHT22 humidity
    double tOUT_POROG = 0;
    double tOUT = 0;


    double secFREE_ON = 5;
    double secFREE_OFF = 10;
    bool FREE_START = false;
    high_resolution_clock::time_point t1;

    std::list <MyTimeRangeClass*> listTIMER;


    BananaState(string topic_prefix);
    ~BananaState();

    int initMqtt(mqtt::async_client *cli);
    int sendMqtt(mqtt::async_client *cli, std::string topic, std::string message);
    int updateState(std::string topic, std::string message);

    void log() {
        LOG(INFO) << "----------BananaState-------------" << endl;
        LOG(INFO) << "PIN_LAMP_STATE:" << PIN_LAMP_STATE << " PIN_PUMP_STATE:" << PIN_PUMP_STATE << " PIN_FAN_STATE:" << PIN_FAN_STATE << endl;
        LOG(INFO) << "PIN_COND_STATE:" << PIN_COND_STATE << " PIN_COMPRES_STATE:" << PIN_COMPRES_STATE << " PIN_FREE_STATE:" << PIN_FREE_STATE << endl;
        LOG(INFO) << "tLAMP:" << tLAMP << " tLAMP_MIN:" << tLAMP_MIN << " tLAMP_MAX:" << tLAMP_MAX << " tLAMP_ALARM:" << tLAMP_ALARM << endl;
        LOG(INFO) << "tIN:" << tIN << " tIN_MIN:" << tIN_MIN << " tIN_MAX:" << tIN_MAX << endl;
        LOG(INFO) << "tOUT:" << tOUT << " tOUT_POROG:" << tOUT_POROG << endl;
        LOG(INFO) << "FREE_START: " << FREE_START << " secFREE_ON:" << secFREE_ON << " secFREE_OFF:" << secFREE_OFF << endl;
        LOG(INFO) << "----------/BananaState-------------" << endl;
    }

private:
    double getDoubleValue(Json::Value jsonValue, double dfltValue);
    double getBoolValue(Json::Value jsonValue, double dfltValue);
    void setSchedulerValue(Json::Value jsonValue);

};


#endif //BANANARELAY_BANANASTATE_H
