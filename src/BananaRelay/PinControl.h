//
// Created by Antonchikov Alexander on 29.01.2021.
//

#ifndef BANANARELAY_PINCONTROL_H
#define BANANARELAY_PINCONTROL_H
#include <string>
#include "BananaState.h"

class PinControl {
public:
    static int initWiringPi();
    static int updateWiringPi(std::string topic, std::string message, BananaState* bananaState);

};


#endif //BANANARELAY_PINCONTROL_H
