//
// Created by Antonchikov Alexander on 29.01.2021.
//

#include <glog/logging.h>
#include "BananaState.h"
#include "PinControl.h"
#include <wiringPi.h>
#include <string>
#include <map>
#include <iostream>
#include <chrono>
#include <jsoncpp/json/json.h> // or jsoncpp/json.h , or json/json.h etc.
#include <fstream>

using namespace std;
using namespace chrono;

BananaState::BananaState(string topic_prefix) {
    this->TOPIC_PREFIX = topic_prefix;
    struct stat info;
    const char*  path = STATE_FILE.c_str();
    int statRC = stat( path, &info );

    // File not exist
    if( statRC != 0 ) {
        string error = "Banana state file does not exist, create emtpy file " + STATE_FILE;
        LOG(FATAL) << error;
        throw runtime_error(error);
    }
    else {
        Json::Reader reader;
        ifstream status_file(STATE_FILE, ifstream::binary);

        bool parsingSuccessful = reader.parse( status_file, root, false );
        if ( !parsingSuccessful ) {
            stringstream buffer;
            if(status_file.gcount()==0) {
                reader.parse( "{\"FREE\": true}", root );
                LOG(ERROR) << "Empty banana state json, initialize as new {}" << endl;
//                throw runtime_error("Banana stat file is empty. " + STATE_FILE);
            }
            else {
                LOG(INFO) << status_file.rdbuf() << endl;
                LOG(FATAL)  << reader.getFormatedErrorMessages() << endl;
                throw runtime_error("Banana stat file parsing failed. " + STATE_FILE);
            }
        }
        string encoding = root.get("encoding", "UTF-8" ).asString();
        Json::StyledWriter styleWriter;
        string strRoot = styleWriter.write(root);
        cout  << "Banana state file encoding:" << endl;
        LOG(INFO) << encoding << endl;
        cout  << "Banana state file content:" << endl;
        cout  << strRoot << endl;
    }

}

BananaState::~BananaState() {
}

double BananaState::getDoubleValue(Json::Value jsonValue, double dfltValue) {
    try {
        return jsonValue.asDouble();
    } catch (const Json::LogicError& exc) {
        cerr << exc.what() << " " << jsonValue << endl;
        return dfltValue;
    }
}

double BananaState::getBoolValue(Json::Value jsonValue, double dfltValue) {
    try {
        return jsonValue.asBool();
    } catch (const Json::LogicError& exc) {
        cerr << exc.what() << " " << jsonValue << endl;
        return dfltValue;
    }
}

/**
 *
 * @param jsonValue - array of timers
 */
void BananaState::setSchedulerValue(Json::Value jsonValue) {
    listTIMER.clear();
    for (Json::Value::const_iterator outer = jsonValue.begin(); outer != jsonValue.end(); outer++) {
        Json::Value obj = (*outer);
        uint8_t starthour = obj["starthour"].asUInt();
        uint8_t endhour = obj["endhour"].asUInt();
        uint8_t startmin = obj["startmin"].asUInt();
        uint8_t endmin = obj["endmin"].asUInt();
        MyTimeRangeClass *timeRange = new MyTimeRangeClass(starthour, startmin, 0, endhour, endmin, 0);
        listTIMER.push_back(timeRange);
    }
    list<MyTimeRangeClass *>::iterator it;
    for (it = listTIMER.begin(); it != listTIMER.end(); it++) {
//                    LOG(INFO) << (*it) << " ";
        (*it)->printSerial();
    }
}

int BananaState::initMqtt(mqtt::async_client *cli) {
    LOG(INFO)  << "initMqtt" << endl;
//    const std::string TOPIC1 { TOPIC_PREFIX+"/PUMP" };
//    const char* PAYLOAD2 = "off";
//    cli->publish(TOPIC1, PAYLOAD2, strlen(PAYLOAD2), 0, false);

//    std::map<char, std::string>::iterator it;
//    for (it = MQTT_RELAY_TOPIC.begin(); it != MQTT_RELAY_TOPIC.end(); it++)
//    {
//        LOG(INFO) << it->first    // string (key)
//                  << ':'
//                  << TOPIC_PREFIX + it->second   // string's value
//                  << std::endl;
//        const char * c = str.c_str();
//        try{
//            cli->publish( it->second, PAYLOAD2, strlen(PAYLOAD2), 0, false);
//            if(it->second == TOPIC_PREFIX+"/LAMP") PIN_LAMP_STATE = false;
//            if(it->second == TOPIC_PREFIX+"/PUMP") PIN_PUMP_STATE = false;
//            if(it->second == TOPIC_PREFIX+"/FAN") PIN_FAN_STATE = false;
//            if(it->second == TOPIC_PREFIX+"/COND") PIN_COND_STATE = false;
//            if(it->second == TOPIC_PREFIX+"/COMPRES") PIN_COMPRES_STATE = false;
//            if(it->second == TOPIC_PREFIX+"/FREE") PIN_FREE_STATE = false;
//
//        }
//        catch (const mqtt::exception& exc) {
//            LOG(FATAL) << exc.what() << std::endl;
//            return 1;
//        }
//    }

    bool init_bool = false;
    init_bool = (root.isMember("LAMP")) ? getBoolValue(root["LAMP"], PIN_LAMP_STATE) : PIN_LAMP_STATE;
    sendMqtt(cli, TOPIC_PREFIX+"/LAMP", init_bool? "on": "off");

    init_bool = (root.isMember("PUMP")) ? getBoolValue(root["PUMP"], PIN_PUMP_STATE) : PIN_PUMP_STATE;
    sendMqtt(cli, TOPIC_PREFIX+"/PUMP", init_bool? "on": "off");

    init_bool = (root.isMember("FAN")) ? getBoolValue(root["FAN"], PIN_FAN_STATE) : PIN_FAN_STATE;
    sendMqtt(cli, TOPIC_PREFIX+"/FAN", init_bool? "on": "off");

    init_bool = (root.isMember("COND")) ? getBoolValue(root["COND"], PIN_COND_STATE) : PIN_COND_STATE;
    sendMqtt(cli, TOPIC_PREFIX+"/COND", init_bool? "on": "off");

    init_bool = (root.isMember("COMPRES")) ? getBoolValue(root["COMPRES"], PIN_COMPRES_STATE) : PIN_COMPRES_STATE;
    sendMqtt(cli, TOPIC_PREFIX+"/COMPRES", init_bool? "on": "off");

    init_bool = (root.isMember("FREE")) ? getBoolValue(root["FREE"], PIN_FREE_STATE) : PIN_FREE_STATE;
    sendMqtt(cli, TOPIC_PREFIX+"/FREE", init_bool? "on": "off");

    tLAMP_MIN = (root.isMember("TEMP_LAMP_MIN")) ? getDoubleValue(root["TEMP_LAMP_MIN"], tLAMP_MIN) : tLAMP_MIN;
    sendMqtt(cli, TOPIC_PREFIX+"/TEMP_LAMP_MIN", to_string(tLAMP_MIN));

    tLAMP_MAX = (root.isMember("TEMP_LAMP_MAX")) ? getDoubleValue(root["TEMP_LAMP_MAX"], tLAMP_MAX) : tLAMP_MAX;
    sendMqtt(cli, TOPIC_PREFIX+"/TEMP_LAMP_MAX", to_string(tLAMP_MAX));

    tLAMP_ALARM = (root.isMember("TEMP_LAMP_ALARM")) ? getDoubleValue(root["TEMP_LAMP_ALARM"], tLAMP_ALARM) : tLAMP_ALARM;
    sendMqtt(cli, TOPIC_PREFIX+"/TEMP_LAMP_ALARM", to_string(tLAMP_ALARM));

    tIN_MIN = (root.isMember("TEMP_IN_MIN")) ? getDoubleValue(root["TEMP_IN_MIN"], tIN_MIN) : tIN_MIN;
    sendMqtt(cli, TOPIC_PREFIX+"/TEMP_IN_MIN", to_string(tIN_MIN));

    tIN_MAX = (root.isMember("TEMP_IN_MAX")) ? getDoubleValue(root["TEMP_IN_MAX"], tIN_MAX) : tIN_MAX;
    sendMqtt(cli, TOPIC_PREFIX+"/TEMP_IN_MAX", to_string(tIN_MAX));

    tOUT_POROG = (root.isMember("TEMP_OUT_POROG")) ? getDoubleValue(root["TEMP_OUT_POROG"], tOUT_POROG) : tOUT_POROG;
    sendMqtt(cli, TOPIC_PREFIX+"/TEMP_OUT_POROG", to_string(tOUT_POROG));

    secFREE_ON = (root.isMember("FREE_ON")) ? getDoubleValue(root["FREE_ON"], secFREE_ON) : secFREE_ON;
    sendMqtt(cli, TOPIC_PREFIX+"/FREE_ON", to_string(secFREE_ON));

    secFREE_OFF = (root.isMember("FREE_OFF")) ? getDoubleValue(root["FREE_OFF"], secFREE_OFF) : secFREE_OFF;
    sendMqtt(cli, TOPIC_PREFIX+"/FREE_OFF", to_string(secFREE_OFF));

    init_bool = (root.isMember("FREE_START")) ? getBoolValue(root["FREE_START"], FREE_START) : FREE_START;
    sendMqtt(cli, TOPIC_PREFIX+"/FREE_START", init_bool? "on": "off");

    if(root.isMember("LAMP_SCHEDULER")) {
        this->setSchedulerValue(root["LAMP_SCHEDULER"]["timers"]);
        Json::FastWriter fastWriter;
        string sTimers = fastWriter.write(root["LAMP_SCHEDULER"]);
        sendMqtt(cli, TOPIC_PREFIX+"/LAMP_SCHEDULER", sTimers);
    }
    return 0;
}


int BananaState::sendMqtt(mqtt::async_client *cli, std::string topic, std::string message) {

    const char* PAYLOAD2 = message.c_str();
//        const char * c = str.c_str();
    try{
        LOG(INFO) << "Sending message: " << topic + ":" + message << endl;
        cli->publish( topic, PAYLOAD2, strlen(PAYLOAD2), 0, false);
    }
    catch (const mqtt::exception& exc) {
        std::cerr << exc.what() << std::endl;
        return 1;
    }
    return 0;
}

int BananaState::updateState(string topic, string message) {
    try {

        if (topic == TOPIC_PREFIX+"/LAMP") {
            PinControl::updateWiringPi( topic, message, this);
            root["LAMP"] = Json::Value(PIN_LAMP_STATE);
        }
        if (topic == TOPIC_PREFIX+"/PUMP") {
            PinControl::updateWiringPi( topic, message, this);
            root["PUMP"] = Json::Value(PIN_PUMP_STATE);
        }
        if (topic == TOPIC_PREFIX+"/FAN") {
            PinControl::updateWiringPi( topic, message, this);
            root["FAN"] = Json::Value(PIN_FAN_STATE);
        }
        if (topic == TOPIC_PREFIX+"/COND") {
            PinControl::updateWiringPi( topic, message, this);
            root["COND"] = Json::Value(PIN_COND_STATE);
        }
        if (topic == TOPIC_PREFIX+"/COMPRES") {
            PinControl::updateWiringPi( topic, message, this);
            root["COMPRES"] = Json::Value(PIN_COMPRES_STATE);
        }
        if (topic == TOPIC_PREFIX+"/FREE") {
            PinControl::updateWiringPi( topic, message, this);
            root["FREE"] = Json::Value(PIN_FREE_STATE);
        }


        if (topic == TOPIC_PREFIX+"/TEMP_LAMP_MAX") {
            tLAMP_MAX = stod(message);
            root["TEMP_LAMP_MAX"] = Json::Value(tLAMP_MAX);
        }
        if (topic == TOPIC_PREFIX+"/TEMP_LAMP_MIN") {
            tLAMP_MIN = stod(message);
            root["TEMP_LAMP_MIN"] = Json::Value(tLAMP_MIN);
        }
        if (topic == TOPIC_PREFIX+"/TEMP_LAMP_ALARM") {
            tLAMP_ALARM = stod(message);
            root["TEMP_LAMP_ALARM"] = Json::Value(tLAMP_ALARM);
        }
        if (topic == TOPIC_PREFIX+"/TEMP_IN_MAX") {
            tIN_MAX = stod(message);
            root["TEMP_IN_MAX"] = Json::Value(tIN_MAX);
        }
        if (topic == TOPIC_PREFIX+"/TEMP_IN_MIN") {
            tIN_MIN = stod(message);
            root["TEMP_IN_MIN"] = Json::Value(tIN_MIN);
        }
        if (topic == TOPIC_PREFIX+"/TEMP_OUT_POROG") {
            tOUT_POROG = stod(message);
            root["TEMP_OUT_POROG"] = Json::Value(tOUT_POROG);
        }

        if (topic == TOPIC_PREFIX+"/FREE_START") {
            if (message == "on") {
                FREE_START = true;
                root["FREE_START"] = true;
                t1 = high_resolution_clock::now();
            } else if (message == "off") {
                FREE_START = false;
                root["FREE_START"] = false;
            }
        }
        if (topic == TOPIC_PREFIX+"/FREE_ON") {
            secFREE_ON = stod(message);
            root["FREE_ON"] = Json::Value(secFREE_ON);
        }
        if (topic == TOPIC_PREFIX+"/FREE_OFF") {
            secFREE_OFF = stod(message);
            root["FREE_OFF"] = Json::Value(secFREE_OFF);
        }
        if (topic == TOPIC_PREFIX+"/DS18B20/1/TEMP") {
            tLAMP = stod(message);
        }
        if (topic == TOPIC_PREFIX+"/DS18B20/0/TEMP") {
            tOUT = stod(message);
        }
        if (topic == TOPIC_PREFIX+"/DHT22/TEMP") {
            tIN = stod(message);
        }

        if (topic == TOPIC_PREFIX+"/LAMP_SCHEDULER") {
            Json::Value root;
            Json::Reader reader;
            bool parsingSuccessful = reader.parse(message, root);
            if (!parsingSuccessful) {
                LOG(INFO) << "Error parsing the string" << endl;
            }
            Json::FastWriter fastWriter;
            string output = fastWriter.write(root["timers"]);
            LOG(INFO) << "1: " << output << endl;
            Json::Value timers = root["timers"];

            setSchedulerValue(root["timers"]);
            this->root["LAMP_SCHEDULER"] = root;
        }



    } catch (const std::exception& ex) {
        LOG(WARNING) << "Error updateState " << topic << " " << message << ex.what() << endl;
        return 1;
    }

    struct stat info;
    const char*  path = STATE_FILE.c_str();
    int statRC = stat( path, &info );
// File not exist
    if( statRC != 0 )
        throw runtime_error("State file does not exist, create empty file with command touch " + STATE_FILE);

    Json::StyledWriter sw;
    sw.write(root);
    // output to a file
    ofstream os;
    os.open(STATE_FILE);
    os << sw.write(root);
    os.close();
//        cerr << "Error: writing file " + STATE_FILE << endl;

    return 0;
}

