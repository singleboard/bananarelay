#ifndef DHT22_H
#define DHT22_H

#include <string>
using namespace std;
struct hcfType {
    string temp;
    string hum;
};

hcfType read_dht_data();

#endif // DHT22_H