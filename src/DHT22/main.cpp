// systemctl status dht22.service
// g++ -Wall main.cpp dht22.cpp -o dht22 -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi

#include <glog/logging.h>
#include <gflags/gflags.h>

#include "dht22.h"
#include <wiringPi.h>

#include <sstream>

#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>	// For sleep
#include <atomic>
#include <chrono>
#include <cstring>

#include "mqtt/async_client.h"

DEFINE_string(mqtt_server, "tcp://localhost:1883", "Define mqtt server address 'tcp://localhost:1883'");
DEFINE_string(mqtt_client_id, "dht22", "Define mqtt client id, must be unique");
DEFINE_int32(wiringpi_pin, 29, "Define wiringPi number for signal");
DEFINE_string(topic_prefix, "BANANA", "Define wiringPi number for signal");
DEFINE_int32(sleep_time_sec, 5, "Define time interval between measurements in seconds");

using namespace std;

//const string DFLT_SERVER_ADDRESS	{ "tcp://localhost:1883" };
//const string DFLT_CLIENT_ID		{ "async_DHT22" };
//const int DFLT_TIMEOUT_SEC = { 5 };


const int  QOS = 1;

const auto TIMEOUT = std::chrono::seconds(10);

static int sendMqtt(mqtt::async_client *cli, std::string topic, std::string message) {
    const char* PAYLOAD = message.c_str();
    try{
        LOG(INFO) << "Sending message: " << topic + ":" + message;
        cli->publish( topic, PAYLOAD, strlen(PAYLOAD), 0, false);
    }
    catch (const mqtt::exception& exc) {
        LOG(ERROR) << exc.what();
        throw runtime_error("Error sending  topic" + topic + " message: " + message);
    }
    return 0;
}

int main(int argc, char* argv[])
{
    gflags::ParseCommandLineFlags(&argc, const_cast<char***>(&argv), true);
//    FLAGS_logtostderr=1;
    FLAGS_alsologtostderr=1;
    FLAGS_logbufsecs = 0;
    google::InitGoogleLogging(to_string( FLAGS_alsologtostderr).c_str() );
    if(FLAGS_log_dir.empty())
        FLAGS_log_dir = "/var/log/banana-relay";
    //Sets the output directory and prefix for logs of a specific severity level. The first parameter is the log level, and the second parameter indicates the output directory and log filename prefix
    google::SetLogDestination(google::GLOG_INFO, (string(FLAGS_log_dir) + "/banana-dht22_info_").c_str());
    google::SetLogDestination(google::GLOG_FATAL, (string(FLAGS_log_dir) + "/banana-dht22_error_").c_str());
    //Add an extension after the middle level of the log file name. For all severity levels
    google::SetLogFilenameExtension("log_");
    //Logs larger than the specified level are output to standard output
    google::SetStderrLogging(google::GLOG_ERROR);
    FLAGS_colorlogtostderr = true;  // Set log color
    FLAGS_logbufsecs = 0;  // Set log output speed(s)
    FLAGS_max_log_size = 1024;  // Set max log file size
    FLAGS_stop_logging_if_full_disk = true;  // If disk is full

    LOG(INFO) << "FLAGS_log_dir" << FLAGS_log_dir;
    LOG(INFO) << "FLAGS_mqtt_server" << FLAGS_mqtt_server;
    LOG(INFO) << "FLAGS_mqtt_client_id" << FLAGS_mqtt_client_id;
    LOG(INFO) << "FLAGS_wiringpi_pin" << FLAGS_wiringpi_pin;
    LOG(INFO) << "FLAGS_topic_prefix" << FLAGS_topic_prefix;
    LOG(INFO) << "FLAGS_sleep_time_sec" << FLAGS_sleep_time_sec;

    const string TOPIC_TEMP { FLAGS_topic_prefix + "/DHT22/TEMP" };
    const string TOPIC_HUM { FLAGS_topic_prefix + "/DHT22/HUM" };

    if (wiringPiSetup() == -1) {
        LOG(FATAL) << "Wiring Pi initialization Faild";
        exit(1);
    }

    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(20);
    connOpts.set_clean_session(true);
    mqtt::async_client cli(FLAGS_mqtt_server, FLAGS_mqtt_client_id);

    try {
        cout << "Connecting to the MQTT server..." << flush;
        cli.connect(connOpts)->wait();
//        cli.start_consuming();
//        cli.subscribe(TOPIC, QOS)->wait();
        cout << "OK" << endl;
    }
    catch (const mqtt::exception& exc) {
        LOG(FATAL) << exc.what();
        return 1;
    }

    while(true) {
        hcfType hcf = read_dht_data();
        if(hcf.temp.length() > 0 ) {
            try {
                sendMqtt(&cli, TOPIC_TEMP, to_string(stod(hcf.temp)));
                sendMqtt(&cli, TOPIC_HUM, to_string(stod(hcf.hum)));
            }
            catch (const mqtt::exception& exc) {
                LOG(ERROR) << exc.what() << endl;
                LOG(ERROR) << "Restart server if it repeats" << endl;
            }
        }
        LOG(INFO) << "Waiting..." << FLAGS_sleep_time_sec << " seconds" << endl;
        this_thread::sleep_for(chrono::seconds(FLAGS_sleep_time_sec));
    }

    try {
        // Disconnect
        cout << "\nShutting down and disconnecting from the MQTT server..." << flush;
        cli.disconnect()->wait();
        cout << "OK" << endl;
	}
	catch (const mqtt::exception& exc) {
		cerr << exc.what() << endl;
		return 1;
	}

}

