#!/bin/bash

systemctl disable banana-dht22.service && systemctl stop banana-dht22.service && rm /etc/systemd/system/banana-dht22.service

systemctl disable banana-ds18b20.service && systemctl stop banana-ds18b20.service && rm /etc/systemd/system/banana-ds18b20.service

systemctl disable banana-relay.service && systemctl stop banana-relay.service && rm /etc/systemd/system/banana-relay.service

#systemctl stop banana-relay.timer && systemctl disable banana-relay.timer && rm /etc/systemd/system/banana-relay.timer

rm /usr/local/bin/banana-relay
rm /usr/local/bin/banana-dht22
rm /usr/local/bin/banana-ds18b20

exit 0