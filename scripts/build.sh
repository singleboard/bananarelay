#!/bin/bash

mkdir bin
# build banana-relay
RUN="g++ -Wall src/BananaRelay/BananaRelay.cpp src/BananaRelay/MyTime.cpp src/BananaRelay/BananaState.cpp src/BananaRelay/PinControl.cpp \
  -o bin/banana-relay \
  -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi -ljsoncpp -lglog -lgflags"
echo $RUN
eval $RUN
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "ERROR execution: $RUN"
    exit 1
fi

# build banana-dht22
RUN="g++ -Wall src/DHT22/main.cpp src/DHT22/dht22.cpp -o bin/banana-dht22 -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi -lglog -lgflags"
echo $RUN
eval $RUN
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "ERROR execution: $RUN"
    exit 1
fi

# build banana-ds18b20
RUN="g++ -Wall src/DS18B20/main.cpp src/DS18B20/OneWire.cpp -o bin/banana-ds18b20 -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi -lglog -lgflags"
echo $RUN
eval $RUN
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "ERROR execution: $RUN"
    exit 1
fi

exit 0
