#!/bin/bash

mkdir /etc/banana-relay
rm -rf /var/log/banana-relay
mkdir /var/log/banana-relay
cd scripts/banana-state.json /etc/banana-relay/banana-state.json

echo "Copy bin files"
cp bin/banana-dht22 /usr/local/bin/banana-dht22
cp bin/banana-ds18b20 /usr/local/bin/banana-ds18b20
cp bin/banana-relay /usr/local/bin/banana-relay

echo "Copy service files"
cp scripts/services/banana-dht22.service /etc/systemd/system/banana-dht22.service
cp scripts/services/banana-ds18b20.service /etc/systemd/system/banana-ds18b20.service
cp scripts/services/banana-relay.service /etc/systemd/system/banana-relay.service
#TODO: Timer not ready yet
#cp scripts/services/banana-relay.timer /etc/systemd/system/banana-relay.timer



RUN="systemctl start banana-dht22.service && systemctl enable banana-dht22.service"
echo $RUN & eval $RUN
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "ERROR execution: $RUN"
    exit 1
fi

RUN="systemctl start banana-ds18b20.service && systemctl enable banana-ds18b20.service"
echo $RUN & eval $RUN
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "ERROR execution: $RUN"
    exit 1
fi

RUN="systemctl start banana-relay.service && systemctl enable banana-relay.service"
echo $RUN & eval $RUN
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "ERROR execution: $RUN"
    exit 1
fi

#RUN="systemctl start banana-relay.timer && systemctl enable banana-relay.timer"
#echo $RUN & eval $RUN
#RESULT=$?
#if [ $RESULT -ne 0 ]; then
#    echo -e "ERROR execution: $RUN"
#    exit 1
#fi

# systemctl list-timers --all
exit 0